package id.bootcamp.batch330android.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import id.bootcamp.batch330android.R

class RegistrationResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_result)

        // Ambil data yang dikirim dari activity sebeleumnya
        val firstName = intent.extras?.getString("keyFirstName")
        val lastName = intent.extras?.getString("keyLastName")
        val email = intent.extras?.getString("keyEmail")
        val address = intent.extras?.getString("keyAddress")
        val gender = intent.extras?.getString("keyGender")
        val language = intent.extras?.getStringArrayList("keyListLanguage")
        val state = intent.extras?.getString("keyState")

        // Ambil object dari layout
        val textFirstName = findViewById<TextView>(R.id.txtFirstName)
        val textLastName = findViewById<TextView>(R.id.txtLastName)
        val textGender = findViewById<TextView>(R.id.txtGender)
        val textEmail = findViewById<TextView>(R.id.txtEmail)
        val textAddress = findViewById<TextView>(R.id.txtAddress)
        val textState = findViewById<TextView>(R.id.txtState)
        val textLanguage = findViewById<TextView>(R.id.txtLanguage)

        // Set Text data dari Activity Sebelumnya
        textFirstName.text = "First Name : $firstName"
        textLastName.text = "Last Name : $lastName"
        textAddress.text = "Address : $address"
        textLanguage.text = "Language : $language"
        textGender.text = "Gender : $gender"
        textEmail.text = "Email : $email"
        textState.text = "State : $state"

    }
}