package id.bootcamp.batch330android.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Spinner
import id.bootcamp.batch330android.R
import android.widget.Toast
import id.bootcamp.batch330android.utils.isValidEmail

class RegistrationFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_form)

        // Ambil semua object yang ada di form
        val etFirstName = findViewById<EditText>(R.id.etFirstName)
        val etLastName = findViewById<EditText>(R.id.etLastName)
        val radioMale = findViewById<RadioButton>(R.id.radioMale)
        val radioFemale = findViewById<RadioButton>(R.id.radioFemale)
        val cbIndonesia = findViewById<CheckBox>(R.id.cbIndonesia)
        val cbEnglish = findViewById<CheckBox>(R.id.cbEnglish)
        val etEmail = findViewById<EditText>(R.id.etEmail)
        val etAddress = findViewById<EditText>(R.id.etAddress)
        val spinnerState = findViewById<Spinner>(R.id.spinnerState)
        val btnSubmit = findViewById<Button>(R.id.btnSubmit)

        // Ambil array string dari resource
        val states = resources.getStringArray(R.array.states)

        // Mengisi Isi Spinner / Dropdown
        val adapter = ArrayAdapter(
            this, android.R.layout.simple_list_item_1, states
        )
        spinnerState.adapter = adapter

        // Buat clickListener button submit
        btnSubmit.setOnClickListener {
            // Ambil value dari form
            // Edit Text
            val firstName = etFirstName.text.toString()
            val lastName = etLastName.text.toString()
            val email = etEmail.text.toString()
            val address = etAddress.text.toString()

            // Radio Button
            var gender = ""
            if (radioMale.isChecked) {
                gender = "Male"
            } else if (radioFemale.isChecked) {
                gender = "Female"
            }

            // CheckBox
            var listLanguage = ArrayList<String>()
            if (cbIndonesia.isChecked) {
                listLanguage.add("Indonesia")
            }
            if (cbEnglish.isChecked) {
                listLanguage.add("English")
            }

            // Spinner
            val state = spinnerState.selectedItem.toString()

            // Logic Validation
            // First Name tidak boleh kosong
            if (firstName.isBlank()) {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "First Name tidak boleh kosong",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            // Email harus valid
            if (!isValidEmail(email)) {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Email tidak valid",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            // Address tidak boleh kosong
            if (address.isBlank()) {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Address tidak boleh kosong",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            // Gender harus dipilih
            if (gender == "") {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Gender wajib di isi",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            // Language harus dipilih minimal 1
            if (listLanguage.isEmpty()) {
                Toast.makeText(
                    this@RegistrationFormActivity,
                    "Language minimal 1",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            val intent = Intent(this, RegistrationResultActivity::class.java)
            intent.putExtra("keyFirstName", firstName)
            intent.putExtra("keyLastName", lastName)
            intent.putExtra("keyEmail", email)
            intent.putExtra("keyAddress", address)
            intent.putExtra("keyGender", gender)
            intent.putExtra("keyListLanguage", listLanguage)
            intent.putExtra("keyState", state)
            startActivity(intent)
        }

    }
}