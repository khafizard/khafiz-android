package id.bootcamp.batch330android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import id.bootcamp.batch330android.activitystack.Stack1Activity
import id.bootcamp.batch330android.registration.RegistrationFormActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Set LAYOUT
        setContentView(R.layout.activity_main)

        //Ambil object button
        val btnStackActivity = findViewById<Button>(R.id.btnActivityStack)
        val btnRegister = findViewById<Button>(R.id.btnRegister)
        //Atur button setelah di click
        btnStackActivity.setOnClickListener {
            //Perintah munculin toast
//            Toast.makeText(this, "Button Stack Activity diklik", Toast.LENGTH_SHORT)
//                .show()
            // Buat Object Intent
            // Untuk memberitahukan navigasi dari apa ke apa
            val intent = Intent(this, Stack1Activity::class.java)
            startActivity(intent)
        }
        btnRegister.setOnClickListener {
            val intent = Intent(this, RegistrationFormActivity::class.java)
            startActivity(intent)
        }





        Log.d("activity_lifecycle", "OnCreate Terpanggil")

    }

    override fun onStart() {
        super.onStart()
        Log.d("activity_lifecycle", "OnStart Terpanggil")
    }

    override fun onResume() {
        super.onResume()
        Log.d("activity_lifecycle", "OnResume Terpanggil")
    }

    override fun onPause() {
        super.onPause()
        Log.d("activity_lifecycle", "OnPause Terpanggil")
    }

    override fun onStop() {
        super.onStop()
        Log.d("activity_lifecycle", "OnStop Terpanggil")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("activity_lifecycle", "OnDestroy Terpanggil")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("activity_lifecycle", "OnRestart Terpanggil")
    }
}