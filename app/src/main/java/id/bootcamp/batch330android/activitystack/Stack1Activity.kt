package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import id.bootcamp.batch330android.R

class Stack1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack1)

        val buttonStack1Activity = findViewById<Button>(R.id.btnGoStack2)

        buttonStack1Activity.setOnClickListener {
//            Toast.makeText(this, "Button Stack 1 Activity diklik", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, Stack2Activity::class.java)
            startActivity(intent)

        }
    }
}