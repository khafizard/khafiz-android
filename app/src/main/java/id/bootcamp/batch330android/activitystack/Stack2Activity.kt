package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import id.bootcamp.batch330android.R
import android.widget.Toast
import id.bootcamp.batch330android.MainActivity


class Stack2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack2)

        val buttonStack2Activity = findViewById<Button>(R.id.btnGoMainMenu)

        buttonStack2Activity.setOnClickListener {
//            Toast.makeText(this, "Button Stack 2 Activity diklik", Toast.LENGTH_SHORT).show()

            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }
}